import React, { Component } from 'react';
import { View, Text, StatusBar } from 'react-native';
import { NativeRouter, Route, Link } from 'react-router-native';
import Logo from './components/Logo';
import Result from './components/Result';
import Nav from './components/Nav';
import NamotavacForm from './components/NamotavacForm';
import Spotreba from './components/Spotreba';
import Zakazky from './components/Zakazky';

class App extends Component {
  render() {
    return (
      <NativeRouter>
        <View>
          <StatusBar backgroundColor="#1fbad8" />
          <Route path='*' component={Logo} />
          <Route exact path='/' component={Nav} />
          <Route path='/namotavacform' component={NamotavacForm} />
          <Route path='/spotreba' component={Spotreba} />
          <Route path='/zakazky' component={Zakazky} />
        </View>
      </NativeRouter>
    );
  }
}

export default App;
