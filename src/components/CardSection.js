import React from 'react';
import { View, StyleSheet } from 'react-native';

const CardSection = (props) => {
  const { containerStyle } = styles;
  const { style } = props;
  const combineStyles = StyleSheet.flatten([containerStyle, style]);
  return (
    <View style={combineStyles}>
      {props.children}
    </View>
  );
};

const styles = StyleSheet.create({
  containerStyle: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    paddingRight: 30,
    borderBottomWidth: 1,
    borderBottomColor: '#ccc'
  }
});

export default CardSection;
