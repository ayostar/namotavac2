import React, { Component } from 'react';
import { Animated, View, Image } from 'react-native';
import { Link } from 'react-router-native';
import Icon from 'react-native-vector-icons/FontAwesome';

class Logo extends Component {
  state = {
    anim: new Animated.Value(0),
    isSmall: false
  };

  componentWillReceiveProps(nextProps) {
    this.setState({
      isSmall: !this.state.isSmall
    });
    Animated.timing(this.state.anim, {
      toValue: !this.state.isSmall ? 1 : 0,
      duration: 350,
    }).start();
  }

  render() {
    let { anim } = this.state;
    const location = this.props.location;
    const { imageStyle, imageSmallStyle, imageContainerStyle, arrowBack } = styles;

    return (
      <View style={imageContainerStyle}>
        <Animated.Image
          style={{...imageStyle,
            height: anim.interpolate({
              inputRange: [0, 1],
              outputRange: [100, 50]
            }),
            width: anim.interpolate({
              inputRange: [0, 1],
              outputRange: [300, 150]
            })
          }}
          source={require('../img/logo.png')}
        />
        {(location.pathname!=='/') ? <Link style={arrowBack} to='/'><Icon name='arrow-left' size={30} color='#1fbad8' /></Link> : null}
      </View>
    );
  }
}

const styles = {
  imageStyle: {
    height: 100
  },
  imageContainerStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#f5f5f5',
    borderBottomWidth: 1,
    borderBottomColor: '#aaa'
  },
  arrowBack: {
    position: 'absolute',
    left: 35,
    paddingBottom: 0,
    marginLeft: 5,
    marginTop: 30
  }
};

export default Logo;
