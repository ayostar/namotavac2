import React, { Component } from 'react';
import { View, Text, TextInput } from 'react-native';
import CardSection from './CardSection';
import Button from './Button';
import ButtonSection from './ButtonSection';
import Result from './Result';

class NamotavacForm extends Component {
  state = {
    delka: '',
    tloustka: '',
    prumer: '',
    result: '0',
    result2: '0'
  };

  calculateResult(delka, tloustka, prumer) {
    const { result, result2 } = this.state;

    let _delka = parseInt(delka) * 1000; // from metres to milimetres
    let _tloustka = parseInt(tloustka) / 1000; // from mikrometers (mikrons) to milimetres
    let _prumer = parseInt(prumer) * 10; // from centimetres to milimetres

    let vysledek = (Math.sqrt((((4 * _delka) * _tloustka) / Math.PI) + (_prumer * _prumer)) / 10).toFixed(1);
    let vysledek2 = ((vysledek - parseInt(prumer)) / 2).toFixed(1);

    this.setState({
      result: vysledek,
      result2: vysledek2
    });
  }

  onButtonPress(e) {
    const { delka, tloustka, prumer } = this.state;
    e.preventDefault();

    if ( (delka != '') && (tloustka != '') && (prumer != '') ) {
      this.calculateResult(delka, tloustka, prumer);
    } else {
      console.log('spatne zadana hodnota!');
    }
  }

  render() {
    const { labelStyle, containerStyle, textInputStyle, metricStyle, specialMetricStyle } = styles;
    return (
      <View>
        <Result result={[this.state.result, this.state.result2]} type={['Průměr návinu', 'Poloměr návinu']} units='cm'/>
        <View style={containerStyle}>
          <CardSection>
            <Text style={labelStyle}>Délka návinu:</Text>
            <TextInput
              style={textInputStyle}
              placeholder="0"
              keyboardType="numeric"
              onFocus={() => this.setState({ delka: '' })}
              onChangeText={delka => this.setState({ delka })}
              value={this.state.delka}
            />
            <Text style={metricStyle}>m</Text>
          </CardSection>
          <CardSection>
            <Text style={labelStyle}>Tloušťka materiálu:</Text>
            <TextInput
              style={textInputStyle}
              placeholder="0"
              keyboardType="numeric"
              onFocus={() => this.setState({ tloustka: '' })}
              onChangeText={tloustka => this.setState({ tloustka })}
              value={this.state.tloustka}
            />
          <Text style={metricStyle}>&micro;m</Text>
          </CardSection>
          <CardSection>
            <Text style={labelStyle}>Průměr dutinky:</Text>
            <TextInput
              style={textInputStyle}
              placeholder="0"
              keyboardType="numeric"
              onFocus={() => this.setState({ prumer: '' })}
              onChangeText={prumer => this.setState({ prumer })}
              value={this.state.prumer}
            />
          <Text style={specialMetricStyle}>cm</Text>
          </CardSection>
          <ButtonSection>
            <Button onPress={this.onButtonPress.bind(this)}>Vypočítej</Button>
          </ButtonSection>
        </View>
      </View>
    );
  }
}

const styles = {
  labelStyle: {
    color: '#000',
    flex: 1,
    paddingLeft: 20,
    fontSize: 16
  },
  containerStyle: {
    // borderColor: '#000',
    // borderWidth: 1,
    marginLeft: 10,
    marginRight: 10,
    marginTop: 5,
  },
  textInputStyle: {
    fontSize: 18,
    width: 75,
    textAlign: 'right',
    paddingRight: 15
  },
  metricStyle: {
    width: 30
  },
  specialMetricStyle: {
    fontSize: 14,
    width: 30
  }
};

export default NamotavacForm;
