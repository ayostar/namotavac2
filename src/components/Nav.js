import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { Link } from 'react-router-native';

class Nav extends Component {
  render() {
    const { navItem, navContainer, textStyle, arrowBack } = styles;

    return (
      <View style={navContainer}>
        <Link style={navItem} to='/namotavacform'>
          <Text style={textStyle}>Průměr návinu</Text>
        </Link>
        <Link style={navItem} to='/spotreba'>
          <Text style={textStyle}>Spotřeba materiálu</Text>
        </Link>
        <Link style={navItem} to='/zakazky'>
          <Text style={textStyle}>Zakázky</Text>
        </Link>
      </View>
    );
  }
}

const styles = {
  navContainer: {
    marginTop: 20,
    paddingLeft: 50,
    paddingRight: 50
  },
  navItem: {
    backgroundColor: '#1fbad8',
    borderRadius: 5,
    borderWidth: 1,
    borderColor: '#1fbad8',
    marginTop: 20,
    marginLeft: 5,
    marginRight: 5
  },
  textStyle: {
    alignSelf: 'center',
    color: '#fff',
    fontSize: 16,
    fontWeight: '600',
    paddingTop: 20,
    paddingBottom: 20
  }
};

export default Nav;
