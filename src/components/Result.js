import React from 'react';
import { View, Text } from 'react-native';

const Result = ({ result, type, units }) => {
  const { titleStyle, containerStyle, resultStyle, itemRight, itemLeft } = styles;
  const Item = type.map((item, key) =>
      <View key={key} style={(key === 0) ? ((result.length === 1) ? itemRight : itemLeft) : itemRight}>
        <Text style={titleStyle}>{item}:</Text>
        <Text style={resultStyle}>{result[key]} {units}</Text>
      </View>
  );
  return (
    <View elevation={2} style={containerStyle}>
      {Item}
    </View>
  );
};

const styles = {
  titleStyle: {
    fontSize: 16
  },
  itemLeft: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    borderRightWidth: 1,
  },
  itemRight: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    borderRightWidth: 0,
  },
  containerStyle: {
    flexDirection: 'row',
    borderColor: '#000',
    borderWidth: 1,
    borderRadius: 5,
    marginTop: 20,
    marginLeft: 10,
    marginRight: 10,
    padding: 5
  },
  resultStyle: {
    fontSize: 22,
    fontWeight: 'bold',
    color: '#1fbad8'
  }
};

export default Result;
