import React, { Component } from 'react';
import { View, Text, TextInput } from 'react-native';
import CardSection from './CardSection';
import Button from './Button';
import ButtonSection from './ButtonSection';
import Result from './Result';

class Spotreba extends Component {

  state = {
    spotreba: '',
    hmotnost: '',
    result: '0'
  };

  calculateResult(spotreba, hmotnost) {
    const { result } = this.state;

    let _spotreba = parseInt(spotreba);
    let _hmotnost = parseInt(hmotnost);

    let vysledek = this.decihours(_hmotnost / _spotreba);
    let hodiny = Math.trunc(vysledek);
    let minuty = vysledek.toString().substr(hodiny.toString().length + 1, 2);
    switch (minuty.length) {
      case 2: minuty += ' min';
        break;
      case 1: minuty += '0 min';
        break;
    }
    if (hodiny === 0) {
      if (minuty.length === 0) {
        this.setState({ result: `..méně než minuta!` })
      } else if (parseInt(minuty) < 10) {
        this.setState({ result: `${minuty.substr(1)}`});
      } else {
        this.setState({ result: `${minuty}` });
      }
    } else {
      this.setState({ result: `${hodiny} h ${minuty}`});
    }
  }

  decihours(time) {
    return ((i) => {
      return (i + (Math.round(((time - i) * 60), 10) / 100));
    })(parseInt(time, 10));
  }

  onButtonPress(e) {
    const { spotreba, hmotnost } = this.state;
    e.preventDefault();

    if ( (spotreba != '') && (hmotnost != '') ) {
      this.calculateResult(spotreba, hmotnost);
    } else {
      console.log('spatne zadana hodnota!');
    }
  }

  render() {
    const { labelStyle, containerStyle, textInputStyle, metricStyle, specialMetricStyle } = styles;
    return (
      <View>
        <Result result={[this.state.result]} type={['Celkový čas']} units='' />
        <View style={containerStyle}>
          <CardSection>
            <Text style={labelStyle}>Spotřeba materiálu:</Text>
            <TextInput
              style={textInputStyle}
              placeholder="0"
              keyboardType="numeric"
              onFocus={() => this.setState({ spotreba: '' })}
              onChangeText={spotreba => this.setState({ spotreba })}
              value={this.state.spotreba}
            />
            <Text style={specialMetricStyle}>kg/h</Text>
          </CardSection>
          <CardSection>
            <Text style={labelStyle}>Hmotnost:</Text>
            <TextInput
              style={textInputStyle}
              placeholder="0"
              keyboardType="numeric"
              onFocus={() => this.setState({ hmotnost: '' })}
              onChangeText={hmotnost => this.setState({ hmotnost })}
              value={this.state.hmotnost}
            />
          <Text style={metricStyle}>kg</Text>
          </CardSection>
          <ButtonSection>
            <Button onPress={this.onButtonPress.bind(this)}>Vypočítej</Button>
          </ButtonSection>
        </View>
      </View>
    );
  }
}

const styles = {
  labelStyle: {
    color: '#000',
    flex: 1,
    paddingLeft: 20,
    fontSize: 16
  },
  containerStyle: {
    // borderColor: '#000',
    // borderWidth: 1,
    marginLeft: 10,
    marginRight: 10,
    marginTop: 5,
  },
  textInputStyle: {
    fontSize: 18,
    width: 75,
    textAlign: 'right',
    paddingRight: 15
  },
  metricStyle: {
    width: 30
  },
  specialMetricStyle: {
    fontSize: 14,
    width: 30
  }
};

export default Spotreba;
